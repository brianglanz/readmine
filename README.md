READMINE: Suggested template for GitLab READMEs
===============================================

This is an example README file, demonstrating suggested README contents for GitLab projects.

Copy this file into your project and edit it as needed.

[![License](https://img.shields.io/badge/License-CC0-lightgray.svg?style=flat-square)](https://creativecommons.org/publicdomain/zero/1.0/)
[![Latest release](https://img.shields.io/badge/Latest_release-1.0.0-b44e88.svg?style=flat-square)](http://shields.io)

Table of contents
-----------------

* [Introduction](#introduction)
* [Installation](#installation)
* [Usage](#usage)
* [Known issues and limitations](#known-issues-and-limitations)
* [Get help](#get-help)
* [Contribute](#contribute)
* [License](#license)
* [Authors and history](#authors-and-history)
* [Acknowledgments](#acknowledgments)

Introduction
------------
This repository provides both a template for README files and a demonstration of what a README should look like.

The repository includes a README in [Markdown format](https://docs.gitlab.com/ee/user/markdown.html) along with minimal, recommended supporting files including a license, code of conduct, and guidance for contributions.

The structure of this file is based on examining other recommendations and examples for READMEs, as well as contributors' experience creating many open source projects, repositories, and READMEs, as referenced in sections for [Authors and history](#authors-and-history) and [Acknowledgments](#acknowledgments).

The [Introduction](#introduction) section &mdash; which you are reading now &mdash; should provide a brief overview of your project, in many cases with links to resources to help orient readers. Many READMEs begin with [Installation instructions](#installation) as the first section, but introductions are important for readers who are less familiar with your subject. The introduction should be short.

Installation
------------

Begin this section by listing prerequisites &mdash; what may be important for users to have before they can use your project. Consider dependencies and requirements broadly &mdash; for hardware, software including operating system, networking, configuration, and settings.

Next, provide step-by-step instructions for installation. Include command examples that readers can copy-paste, such as:

```bash
a command-line command here
```

Consider adding subsections for different operating systems, for complicated installations, or to make your installation instructions more easily used by readers who are in a hurry or have specific needs.

Usage
-----

This section explains principles behind the README. If this repository were for a software project, as would often be the case, then its [Usage](#usage) section would explain how to run the software, output or behavior to expect, and so on.

The following instructions, for basic operation with the READMINE project's example README, are a demonstration Usage section.

### Basic operation
A suggested approach for using the READMINE project's example README:

1. Copy the [source](README.md) for this file into your repository, and commit it to your version control system
1. Delete the body text, but keep the section headings
1. Write your README content, and commit the new text to your version control system
1. Update your README as your project evolves

The first paragraph in a README, under its title at the top, should summarize your project briefly as shown below:

<p align="center"><img width="75%" src=".graphics/screenshot-top-paragraph.png"></p>

The space between [the first paragraph](#readmine-suggested-template-for-gitlab-readmes) and the [Table of Contents](#table-of-contents) is a good location for [project badges](https://docs.gitlab.com/ee/user/project/badges.html). Badges are a standard way to present condensed pieces of information about your project, consisting of a small image and a URL, pointed to by the image. Badges can quickly communicate project status, version, dependencies, DOIs, maintainers' contact information, and more. Whether you use badges, which badges and colors, their order, and more depend on your project and personal tastes.

The [Introduction](#introduction) and [Use](#use) sections are described above.

In the [Known issues and limitations](#known-issues-and-limitations) section, summarize your project's notable issues and limitations.

The [Get help](#get-help) section should tell users how to contact you and how to report problems they encounter.

The [Contribute](#contribute) section is optional. If your repository is for a project which accepts contributions, then this section is where you can explain to readers and users how to become contributors.

The [License](#license) section should specify usage rights or limitations, and state any copyright asserted on project materials, along with terms of use of the software, files, and other materials in the project repository.

The [Authors and history](#authors-and-history) section should inform readers who the authors are and briefly describe project history, and the [Acknowledgments](#acknowledgments) section can recognize other contributions and the use of other people's software or tools. You may combine these two sections, for example with one section called 'Authors and acknowledgments'.

### Additional options

Some projects need to communicate additional information to users and can benefit from additional sections in the README. Specific instructions will depend on your project or software, and its intended audience, users, and so on. Ask for feedback from users or colleagues to decide what else needs explaining in your README.

Known issues and limitations
----------------------------

In this section, summarize your project's notable issues and limitations. If none are known yet, this section can be omitted or can say something like "none are known at this time." If omitting this section, remember to remove the corresponding entry in the [Table of Contents](#table-of-contents).

Get help
------------

Inform readers how to report problems they encounter, and how to contact you. Use this section to specify means of getting help, whether [the Service Desk issue tracker included with your GitLab project repository](https://docs.gitlab.com/ee/user/project/service_desk.html) (under Issues > Service Desk in the sidebar), or an associated chat, mailing list, email account, or other means.

GitLab's [Service Desk](https://docs.gitlab.com/ee/user/project/service_desk.html) helps you provide efficient email support to your users, who can email you bug reports, feature requests, or general feedback, which will end up in your GitLab project as new issues. You can also respond from within the project.

Contribute
------------

Describe how people can contribute and point them to your contribution guidelines.

[The READMINE contribution guidelines](https://gitlab.com/brianglanz/readmine/blob/master/CONTRIBUTE.md), for example, include sections on [Conduct](https://gitlab.com/brianglanz/readmine/blob/master/CONTRIBUTE.md#conduct), [Coordinating work](https://gitlab.com/brianglanz/readmine/blob/master/CONTRIBUTE.md#coordinate-work), and [Submitting contributions](https://gitlab.com/brianglanz/readmine/blob/master/CONTRIBUTE.md#submit-contributions).

[The READMINE project Code of Conduct](https://gitlab.com/brianglanz/readmine/blob/master/CODE_OF_CONDUCT.md) is specified separately with sections for [Our Pledge](https://gitlab.com/brianglanz/readmine/blob/master/CODE_OF_CONDUCT.md#our-pledge), [Our Standards](https://gitlab.com/brianglanz/readmine/blob/master/CODE_OF_CONDUCT.md#our-standards), [Our Responsibilities](https://gitlab.com/brianglanz/readmine/blob/master/CODE_OF_CONDUCT.md#our-responsibilities), [Scope](https://gitlab.com/brianglanz/readmine/blob/master/CODE_OF_CONDUCT.md#scope), [Enforcement](https://gitlab.com/brianglanz/readmine/blob/master/CODE_OF_CONDUCT.md#enforcement), and [Attribution](https://gitlab.com/brianglanz/readmine/blob/master/CODE_OF_CONDUCT.md#attribution).

License
-------

This README, repository, and READMINE project are distributed under the terms of the [Creative Commons 1.0 Universal license (CC0)](https://creativecommons.org/publicdomain/zero/1.0/). The license applies to this file and other files in [the READMINE repository and project on GitLab](https://gitlab.com/brianglanz/readmine).

This does not mean that you, as a user of this README, must also use a CC0 license. A CC0 license allows you to use any license you wish for your work, regardless of the degree to which it includes this work.

You may also upload copies of relevant licenses to your repository, as done with the [CC0 license for the READMINE project](https://gitlab.com/brianglanz/readmine/blob/master/LICENSE).

Authors and history
-------------------

In this section, list the authors of your project and briefly describe your project's history.

Acknowledgments
---------------

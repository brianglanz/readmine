Contributor Covenant Code of Conduct
====================================

## Our Pledge

In the interest of fostering an open and welcoming environment, we as contributors and maintainers pledge to making participation in our project and our community a harassment-free experience for everyone regardless of age, body size, (dis)ability, ethnicity, gender identity and expression, level of experience, nationality, personal appearance, race, or sexual identity and orientation.

## Our Standards

Behavior that establishes our standard for a positive environment includes:

* Use welcoming and inclusive language
* Be respectful of differing viewpoints and experiences
* Show empathy towards other community members
* Gracefully accept constructive criticism
* Focus on what is best for the community

Unacceptable behavior by participants includes:

* Unwelcome sexualized language, imagery, or other sexual attention or advances
* Trolling, insulting or derogatory comments, and personal attacks
* Public or private harassment
* Publishing others' private information without explicit permission
* Other conduct which could reasonably be considered inappropriate in a professional setting

## Our Responsibilities

Project contributors are responsible for clarifying and exemplifying standards of acceptable behavior, and are expected to take appropriate and fair corrective action in response to unacceptable behavior.

Project contributors have the right and responsibility to remove, edit, or reject comments, commits, code, wiki edits, issues, and other contributions that are not aligned to this Code of Conduct, or to ban temporarily or permanently any contributor for other behaviors that they deem inappropriate, threatening, offensive, or harmful.

## Scope

This Code of Conduct applies both within project spaces and in public spaces when an individual is representing the project or its community. Examples of representing a project or community include using a project-related e-mail address, posting with a related social media or other online account, or acting as a project representative at an online or offline event. Representation of a project may be further defined and clarified by project contributors.

## Enforcement

If a contributor engages in harassing behavior, project team members may take action they deem appropriate, including warning the offender or expelling them from online forums, project resources, online or offline meetings and events, and any project-related activity or resource. The project team is comprised of publicly acknowledged maintainers, organizers, and contributors.

Unacceptable behavior may be reported by contacting the project team. If you are being harassed, notice that someone else is being harassed, or have related concerns, please contact a member of the project team immediately. All complaints will be reviewed and investigated and will result in a response that is deemed necessary and appropriate to the circumstances. The project team is obligated to maintain confidentiality with regard to the reporter of an incident. Further details of specific enforcement policies may be posted separately.

## Attribution

This Code of Conduct was adapted from READMINE's [Code of Conduct for software projects on GitHub](https://github.com/mhucka/readmine/blob/master/CODE_OF_CONDUCT.md), version 1.0.0, which was itself adapted from Electron's [Contributor Covenant Code of Conduct](https://github.com/electron/electron/blob/master/CODE_OF_CONDUCT.md), which itself was adapted from the [Contributor Covenant](http://contributor-covenant.org/version/1/4), version 1.4.

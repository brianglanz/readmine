# Contribution guidelines

Any constructive contributions are welcome, including issues and bug reports, merge requests with code or documentation, and comments with suggestions for improvements.

## Conduct

Everyone is expected to read and respect the [Code of Conduct](CODE_OF_CONDUCT.md) to contribute to this project.

## Coordinate work

Discover or add to plans for this project with its [GitLab Issue Tracker](https://gitlab.com/brianglanz/readmine/issues). 

Please feel free to leave comments, fork, or clone this work, or to contact contributors and share ideas in any way.

## Submit contributions

Fork [the READMINE project on GitLab](https://gitlab.com/brianglanz/readmine/) and create a merge request with your changes, or please feel free to contact contributors directly. 

When you commit and submit changes, please include clear descriptions of your work in commit messages.
